import 'package:flutter/material.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

import 'year_view.dart';

class ScrollingYearsCalendar extends StatefulWidget {
  ScrollingYearsCalendar({
    required this.initialDate,
    required this.firstDate,
    required this.lastDate,
    required this.currentDateColor,
    this.totalMonthPerRow = 3,
    required this.highlightedDates,
    this.highlightedDateColor,
    this.monthNames,
    this.onMonthTap,
    this.monthTitleStyle,
    this.showScrollbar = true,
  })  : assert(!initialDate.isBefore(firstDate),
            'initialDate must be on or after firstDate'),
        assert(!initialDate.isAfter(lastDate),
            'initialDate must be on or before lastDate'),
        assert(!firstDate.isAfter(lastDate),
            'lastDate must be on or after firstDate'),
        assert(
            monthNames == null || monthNames.length == DateTime.monthsPerYear,
            'monthNames must contain all months of the year');

  final DateTime initialDate;
  final DateTime firstDate;
  final DateTime lastDate;
  final Color currentDateColor;
  final List<DateTime> highlightedDates;
  final Color? highlightedDateColor;
  final List<String>? monthNames;
  final Function(int year, int month)? onMonthTap;
  final TextStyle? monthTitleStyle;
  final bool showScrollbar;

  // Determine total month for each row in a year.
  final int totalMonthPerRow;

  @override
  _ScrollingYearsCalendarState createState() => _ScrollingYearsCalendarState();
}

class _ScrollingYearsCalendarState extends State<ScrollingYearsCalendar> {
  final AutoScrollController controller = AutoScrollController(
    //add this for advanced viewport boundary. e.g. SafeArea
    //   viewportBoundaryGetter: () => Rect.fromLTRB(0, 0, 0, MediaQuery.of(context).padding.bottom),

    //choose vertical/horizontal
    axis: Axis.vertical,

    //this given value will bring the scroll offset to the nearest position in fixed row height case.
    //for variable row height case, you can still set the average height, it will try to get to the relatively closer offset
    //and then start searching.
    // suggestedRowHeight: 200
  );

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.endOfFrame.then(
      (_) {
        if (mounted) {
          _scrollToCounter(widget.initialDate.year, useDuration: false);
        }
      },
    );
  }

  Future _scrollToCounter(int year, {bool useDuration = true}) async {
    final int index = year - widget.firstDate.year;
    await controller.scrollToIndex(
      index,
      preferPosition: AutoScrollPosition.begin,
      duration: useDuration
          ? scrollAnimationDuration
          : const Duration(milliseconds: 10),
    );
    controller.highlight(index);
  }

  @override
  Widget build(BuildContext context) {
    final int _itemCount = widget.lastDate.year - widget.firstDate.year + 1;

    final ListView lv = ListView.builder(
        padding: const EdgeInsets.only(bottom: 16.0),
        controller: controller,
        itemCount: _itemCount,
        primary: false,
        itemBuilder: (BuildContext context, int index) {
          final int year = index + widget.firstDate.year;
          return _getYearView(index: index, year: year);
        });

    if (widget.showScrollbar) {
      return Scrollbar(
        thumbVisibility: true,
        interactive: true,
        controller: controller,
        child: lv,
      );
    } else {
      return lv;
    }
  }

  /// Gets a widget with the view of the given year.
  Widget _getYearView({required int year, required int index}) {
    return AutoScrollTag(
      key: ValueKey<int>(index),
      index: index,
      controller: controller,
      child: YearView(
        year: year,
        currentDateColor: widget.currentDateColor,
        highlightedDates: widget.highlightedDates,
        highlightedDateColor: widget.highlightedDateColor,
        monthNames: widget.monthNames,
        onMonthTap: widget.onMonthTap,
        totalMonthPerRow: widget.totalMonthPerRow,
        monthTitleStyle: widget.monthTitleStyle,
      ),
    );
  }
}
