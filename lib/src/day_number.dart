import 'package:flutter/material.dart';
import 'package:scrolling_years_calendar/src/utils/colors.dart';
import 'utils/screen_sizes.dart';

class DayNumber extends StatelessWidget {
  const DayNumber({
    required this.day,
    this.color,
    this.isHoliday,
    this.holidayColor,
  });

  final int day;
  final Color? color;
  final bool? isHoliday;
  final Color? holidayColor;

  @override
  Widget build(BuildContext context) {
    final double size = getDayNumberSize(context);

    return Container(
      width: size,
      height: size,
      alignment: Alignment.center,
      decoration: _decoration(size),
      child: Text(
        day < 1 ? '' : day.toString(),
        textAlign: TextAlign.center,
        style: TextStyle(
          color: _dayColor(),
          fontSize: getDayFontSize(context),
          fontWeight: isHoliday != null && isHoliday!
              ? FontWeight.bold
              : FontWeight.normal,
        ),
      ),
    );
  }

  Color? _dayColor() {
    if (isHoliday != null && isHoliday!) {
      if (holidayColor == null) {
        return Colors.red;
      } else {
        final bool isLight = isLightColor(holidayColor!);
        return isLight ? Colors.black87 : Colors.white;
      }
    }
    return color != null ? Colors.white : Colors.black87;
  }

  BoxDecoration? _decoration(double size) {
    if (isHoliday != null && isHoliday!) {
      return null;
    }

    if (color == null) {
      return null;
    } else {
      return BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(size / 2),
      );
    }
  }
}
