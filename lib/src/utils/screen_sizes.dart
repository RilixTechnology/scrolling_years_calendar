import 'package:flutter/material.dart';

/// Divides screen sizes into three categories.
enum ScreenSizes {
  small,
  medium,
  large,
}

/// Gets the screen size category of the screen.
ScreenSizes screenSize(BuildContext context) {
  final double width = MediaQuery.of(context).size.width;
  if (width < 400.0) {
    return ScreenSizes.small;
  } else if (width < 700) {
    return ScreenSizes.medium;
  } else {
    return ScreenSizes.large;
  }
}

/// Gets the size of the day number widget.
double getDayNumberSize(BuildContext context) {
  switch (screenSize(context)) {
    case ScreenSizes.small:
      return 14.0;
    case ScreenSizes.medium:
      return 20.0;
    case ScreenSizes.large:
      return 22.0;
  }
}

double getDayFontSize(BuildContext context) {
  switch (screenSize(context)) {
    case ScreenSizes.small:
      return 8.0;
    case ScreenSizes.medium:
      return 10.0;
    case ScreenSizes.large:
      return 12.0;
  }
}
