import 'package:flutter/material.dart';

bool isLightColor(Color color) {
  final double grayscale =
      (0.299 * color.red) + (0.587 * color.green) + (0.114 * color.blue);
  return grayscale > 128;
}
