import 'package:flutter/material.dart';
import 'day_number.dart';
import 'month_title.dart';
import 'utils/dates.dart';

class MonthView extends StatelessWidget {
  const MonthView({
    required this.year,
    required this.month,
    required this.padding,
    required this.currentDateColor,
    this.highlightedDates,
    this.highlightedDateColor,
    this.monthNames,
    this.onTap,
    this.titleStyle,
  });

  final int year;
  final int month;
  final double padding;
  final Color currentDateColor;
  final List<DateTime>? highlightedDates;
  final Color? highlightedDateColor;
  final List<String>? monthNames;
  final Function(int year, int month)? onTap;
  final TextStyle? titleStyle;

  @override
  Widget build(BuildContext context) {
    final Widget child = buildMonthView(context);
    if (onTap == null) {
      return child;
    } else {
      return TextButton(
        onPressed: () => onTap!.call(year, month),
        style: TextButton.styleFrom(padding: const EdgeInsets.all(0)),
        child: child,
      );
    }
  }

  Widget buildMonthView(BuildContext context) {
    // width: 7 * getDayNumberSize(context),
    // margin: EdgeInsets.all(padding),
    return Padding(
      padding: EdgeInsets.all(padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          MonthTitle(month: month, monthNames: monthNames, style: titleStyle),
          const SizedBox(height: 4.0),
          buildMonthDays(context),
        ],
      ),
    );
  }

  Widget buildMonthDays(BuildContext context) {
    final List<Row> dayRows = <Row>[];
    final List<DayNumber> dayRowChildren = <DayNumber>[];

    final int daysInMonth = getDaysInMonth(year, month);
    final int firstWeekdayOfMonth = DateTime(year, month, 1).weekday;

    for (int day = 2 - firstWeekdayOfMonth; day <= daysInMonth; day++) {
      Color? color;
      bool holiday = false;
      if (day > 0) {
        final DateTime dt = DateTime(year, month, day);
        color = getDayNumberColor(dt);
        // if today or highlighted day, don't mark as holiday
        if (color == null) {
          holiday = dt.weekday == DateTime.sunday;
        } else {
          holiday = false;
        }
      }

      dayRowChildren.add(DayNumber(day: day, color: color, isHoliday: holiday));

      if ((day - 1 + firstWeekdayOfMonth) % DateTime.daysPerWeek == 0 ||
          day == daysInMonth) {
        dayRows.add(Row(children: List<DayNumber>.from(dayRowChildren)));
        dayRowChildren.clear();
      }
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: dayRows,
    );
  }

  Color? getDayNumberColor(DateTime date) {
    Color? color;
    if (isCurrentDate(date)) {
      color = currentDateColor;
    } else if (highlightedDates != null &&
        isHighlightedDate(date, highlightedDates!)) {
      color = highlightedDateColor;
    }
    return color;
  }
}
