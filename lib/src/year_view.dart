import 'package:flutter/material.dart';
import 'month_view.dart';
import 'year_title.dart';

class YearView extends StatelessWidget {
  const YearView({
    required this.year,
    required this.currentDateColor,
    required this.highlightedDates,
    this.highlightedDateColor,
    this.monthNames,
    this.onMonthTap,
    this.monthTitleStyle,
    this.totalMonthPerRow = 3,
  });

  final int year;
  final Color currentDateColor;
  final List<DateTime> highlightedDates;
  final Color? highlightedDateColor;
  final List<String>? monthNames;
  final Function(int year, int month)? onMonthTap;
  final TextStyle? monthTitleStyle;
  final int totalMonthPerRow;

  double get horizontalMargin => 16.0;

  double get monthViewPadding => 8.0;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: horizontalMargin,
              vertical: 0.0,
            ),
            child: YearTitle(year),
          ),
          Container(
            margin: EdgeInsets.only(
              left: horizontalMargin,
              right: horizontalMargin,
              top: 2.0,
            ),
            child: const Divider(color: Colors.black26),
          ),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: horizontalMargin - monthViewPadding,
              vertical: 0.0,
            ),
            child: buildYearMonths(context),
          ),
        ],
      ),
    );
  }

  Widget buildYearMonths(BuildContext context) {
    final List<Widget> monthRows = <Widget>[];
    final List<MonthView> monthRowChildren = <MonthView>[];

    for (int month = 1; month <= DateTime.monthsPerYear; month++) {
      monthRowChildren.add(
        MonthView(
          year: year,
          month: month,
          padding: monthViewPadding,
          currentDateColor: currentDateColor,
          highlightedDates: highlightedDates,
          highlightedDateColor: highlightedDateColor,
          monthNames: monthNames,
          onTap: onMonthTap,
          titleStyle: monthTitleStyle,
        ),
      );

      if (month % totalMonthPerRow == 0) {
        monthRows.add(
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List<MonthView>.from(monthRowChildren),
          )
        );
        monthRowChildren.clear();
      }
    }

    return Column(children: List<Widget>.from(monthRows));
  }
}
