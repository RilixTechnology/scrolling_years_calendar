<p align="center">
  <img
    src="https://raw.githubusercontent.com/mennorenkens/flutter_scrolling_calendar/master/screenshots/screenshot.png"
    height="400"
  >
</p>

[![Pub Version](https://img.shields.io/pub/v/scrolling_years_calendar)](https://pub.dev/packages/scrolling_years_calendar)

# Flutter Scrolling Calendar

A customizable calendar widget to easily scroll through the years.

## Features

- Choose range of years and the initial year to show.
- Callback on month tap with date information.
- Choose the color of the current day indicator.
- Override the default month names.

